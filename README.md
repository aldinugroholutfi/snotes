# Secure Notes App with React Native 📝📱💻

## Features ✨

- Log in or create an account with biometric authentication to secure your notes.
- Create new notes and edit existing ones.
- User-friendly interface for easy navigation.

## Tech Stack 🛠️

- React Native for cross-platform development.
- React Native Local Authentication using Biometrics.
- Firebase for authentication and database storage.
- Redux for state management.

## Installation 🚀

1. Clone the repository:

 - git clone

2. Install dependencies:

 - npm install

3. Run the app:

 - npx expo start

## Android
<h2>Splash Screen</h2>
<img src="./assets/screenshoot/splashscreen.png" width="300" alt="Splash Screen">
<h2>Login Screen with Fingerprint Biometric</h2>
<img src="./assets/screenshoot/login.png" width="300" alt="Login">
<h2>Login Authentication Fingerprint Biometric</h2>
<img src="./assets/screenshoot/authentication.jpeg" width="300" alt="Authentication">
<h2>Home Screen</h2>
<img src="./assets/screenshoot/homenote.jpeg" width="300" alt="Home Screen">
<h2>Add Note Screen</h2>
<img src="./assets/screenshoot/add.jpeg" width="300" alt="Add Note Screen">
<h2>Detail Note Screen</h2>
<img src="./assets/screenshoot/detail.jpeg" width="300" alt="Detail Note Screen">
