import firebase from "firebase/compat/app";
import "firebase/compat/auth";
import "firebase/compat/firestore";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyDRKKLUf0Vbjz3IMLri9MdPQjn_JM5GT4E",
  authDomain: "enoteapp-67bbd.firebaseapp.com",
  projectId: "enoteapp-67bbd",
  storageBucket: "enoteapp-67bbd.appspot.com",
  messagingSenderId: "863120042190",
  appId: "1:863120042190:web:ff9212a32f0165b94d889f",
  // measurementId: "G-64EP3R5FCE"
};

const app = firebase.initializeApp(firebaseConfig);

export const db = app.firestore();
export const auth = firebase.auth();
