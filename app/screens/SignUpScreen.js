import React, { useState } from "react";
import {
  View,
  StyleSheet,
  TextInput,
  Image,
  TouchableOpacity,
    Alert,
} from "react-native";
import { Input, Button } from "react-native-elements";
import { auth } from "../../config";
import { RFPercentage } from "react-native-responsive-fontsize";
import { useNavigation } from "@react-navigation/native";
import AppButton from "../components/AppButton";
import Colors from "../config/Colors";
import useBiometricAuth from "../hooks/useBiometricAuth";

const SignUpScreen = (props) => {
  const [signupEmail, setSignupEmail] = useState("");
  const [signupPassword, setSignupPassword] = useState("");
  const navigation = useNavigation();
  const { isAuth, isCompatible, isEnrolled, authorize } = useBiometricAuth();
  const handleAuth = async () => {
        const result = await authorize();
        console.log("Auth result: ", result);
        return result;
  };
  const handleSignup = async () => {
      const isAuthorized = await handleAuth();
      if (isAuthorized) {
          auth
              .createUserWithEmailAndPassword(signupEmail, signupPassword)
              .then((userCredential) => {
                  // Handle successful signup
                  console.log("User signed up successfully!", userCredential.user);
                  navigation.replace("LoginScreen");
              })
              .catch((error) => {
                  // Handle signup errors
                  if (error.code === "auth/email-already-in-use") {
                      Alert.alert(
                          "Already Registered",
                          "The email address is already registered."
                      );
                  } else {
                      console.log("Signup error:", error);
                  }
              });
      } else {

      }
  };
  return (
    <View style={styles.container}>
      <View style={styles.logocontainer}>
        <Image
          style={styles.logo}
          source={require("../../assets/images/snotes-logo-new.png")}
        />
      </View>
      {/* Signup inputs */}
      <Input
        label="Email"
        value={signupEmail}
        onChangeText={(text) => setSignupEmail(text)}
      />
      <Input
        label="Password"
        value={signupPassword}
        secureTextEntry
        onChangeText={(text) => setSignupPassword(text)}
      />
      {/* signup */}
      <TouchableOpacity
        onPress={handleSignup}
        style={styles.loginbutton}
        activeOpacity={0.7}
      >
        <AppButton title="Signup" buttonColor={Colors.blue} />
      </TouchableOpacity>

      {/*  <TouchableOpacity*/}
      {/*  onPress={handleAuth}*/}
      {/*  style={styles.loginbutton}*/}
      {/*  activeOpacity={0.7}*/}
      {/*>*/}
      {/*  <AppButton title="Signup" buttonColor={Colors.red} />*/}
      {/*</TouchableOpacity>*/}
      {/* Login*/}
      <TouchableOpacity
        onPress={() => {
          props.navigation.navigate("LoginScreen");
        }}
        style={styles.loginbutton}
        activeOpacity={0.7}
      >
        <AppButton title="Back" buttonColor={Colors.green} />
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    padding: 16,
    marginBottom: RFPercentage(15),
  },
  logocontainer: {
    alignItems: "center",
    justifyContent: "center",
  },
  logo: {
    width: RFPercentage(30),
    height: RFPercentage(30),
  },
  loginbutton: {
    width: "100%",
    justifyContent: "center",
    alignItems: "center",
    marginTop: RFPercentage(1),
  },
});

export default SignUpScreen;
